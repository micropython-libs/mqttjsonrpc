# mqttjsonrpc

Python/Micropython class that provide simple RPC client/server over mqtt using json

## Dataformat

### Request

{"id": "base64randomstring", "resp_top": "response/topic", "fn": "function_name": "fnc_name", args: [], kwargs: {}}

args and argw are optional

### Response

{"id": "same_base64randomstring", "value": ..., "exception": {"type": "TypeError", "msg": "That is wrong type"}}

There should be value OR exception.

## Example

There are examples of rpc server and client functionality in examples subdirectory.