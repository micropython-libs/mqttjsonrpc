import os
import copy
import logging
import os
import asyncio
if os.name == 'nt':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
import time
import subprocess
import platform
import shutil
import signal

import aiomqtt
import pytest
import pytest_asyncio

import mqttjsonrpc
from mqttjsonrpc.amqttrpc import AMqttRpcClient, AMqttRpcServer
from tests.mosquitto import Mosquitto

logger = logging.getLogger(__name__)

SERVER_TOPIC = "server/topic"


class PortRedirect:
    BINARY_NAMES = ['socat', ]
    def __init__(self, listen_port, target_port):
        self.listen_port = listen_port
        self.target_port = target_port

        self.thread = None
        self.process = None
        self.bin_name = None

    def __del__(self):
        if self.process:
            self.stop()

    def start(self):
        for bin_name in self.BINARY_NAMES:
            if shutil.which(bin_name):
                break
        else:
            raise ValueError("Socat binary not found")
        self.bin_name = bin_name
        logger.debug("Tcp redirect started")
        self.process = subprocess.Popen(
            [self.bin_name, f'TCP-LISTEN:{self.listen_port},fork', f'TCP:127.0.0.1:{self.target_port}'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

    def stop(self):
        if self.process:
            logger.debug("Stopping tcp redirect")
            os.system(f"killall {self.bin_name}")
            self.process.wait()
            self.process = None


@pytest.fixture
def basic_json_rpc():
    json_rpc = mqttjsonrpc.jsonrpc.JsonRpc()
    json_rpc.add_func("empty", lambda : None)
    json_rpc.add_func("return_val", lambda x: x)
    json_rpc.add_func("return_val_str", lambda x: str(x))
    json_rpc.add_func("raise_str", lambda x: exec('raise ValueError(f"raise for value {x}")'))
    return json_rpc


@pytest.fixture
def mqtt_broker():
    ret = Mosquitto()
    return ret

class TestIntegration:
#    @pytest.mark.asyncio
#    async def test_not_connected_server(self, basic_json_rpc, mqtt_broker):
#        pass

#    @pytest.mark.asyncio
#    async def test_not_connected_client(self, basic_json_rpc, mqtt_broker):
#        pass

    @pytest.mark.asyncio
    async def test_basic_integration(self, basic_json_rpc, mqtt_broker):
        mqtt_broker.start()
        await asyncio.sleep(1)

        mqtt_params = {"host": "127.0.0.1", "protocol": 3, "port": mqtt_broker.port}
        server = mqttjsonrpc.amqttrpc.AMqttRpcServer(basic_json_rpc, mqtt_params, SERVER_TOPIC)
        await server.start()
        await asyncio.sleep(1)
        client = AMqttRpcClient(basic_json_rpc, mqtt_params, SERVER_TOPIC + "/rpc")
        await client.start()
        await asyncio.sleep(1)

        ret = await client.return_val(44)
        assert ret == 44

        await server.stop()
        await client.stop()
        mqtt_broker.stop_testing()

    @pytest.mark.asyncio
    async def test_bytes_all(self, basic_json_rpc, mqtt_broker):
        mqtt_broker.start()
        await asyncio.sleep(1)

        mqtt_params = {"host": "127.0.0.1", "protocol": 3, "port": mqtt_broker.port}
        server = mqttjsonrpc.amqttrpc.AMqttRpcServer(basic_json_rpc, mqtt_params, SERVER_TOPIC)
        await server.start()
        await asyncio.sleep(1)
        client = AMqttRpcClient(basic_json_rpc, mqtt_params, SERVER_TOPIC + "/rpc")
        await client.start()
        await asyncio.sleep(1)

        test_val = {'x': [b'abc']}
        ret = await client.return_val(copy.deepcopy(test_val))
        assert ret == test_val

        test_val = {'x': [b'abc']}
        ret = await client.return_val_str(copy.deepcopy(test_val))
        assert ret == str(test_val)

        await server.stop()
        await client.stop()
        mqtt_broker.stop_testing()

    @pytest.mark.asyncio
    async def test_server_reconnection(self, basic_json_rpc, mqtt_broker):
        mqtt_broker.start_testing(5, 1)
        await asyncio.sleep(1)

        mqtt_params = {"host": "127.0.0.1", "protocol": 3, "port": mqtt_broker.port}
        server = mqttjsonrpc.amqttrpc.AMqttRpcServer(basic_json_rpc, mqtt_params, SERVER_TOPIC)
        await server.start()
        await asyncio.sleep(1)

        await asyncio.sleep(30)

        await server.stop()
        mqtt_broker.stop_testing()

    @pytest.mark.asyncio
    async def test_reconnection(self, basic_json_rpc, mqtt_broker):
        mqtt_broker.start_testing(5, 1)
        await asyncio.sleep(1)

        mqtt_params = {"host": "127.0.0.1", "protocol": 3, "port": mqtt_broker.port}
        server = mqttjsonrpc.amqttrpc.AMqttRpcServer(basic_json_rpc, mqtt_params, SERVER_TOPIC)
        await server.start()
        await asyncio.sleep(1)
        client = AMqttRpcClient(basic_json_rpc, mqtt_params, SERVER_TOPIC + "/rpc")
        await client.start()
        await asyncio.sleep(1)

        test_cnt = 10_000
        ok_calls = 0
        fail_calls = 0
        for i in range(test_cnt):
            call_ok = False
            while not call_ok:
                try:
                    ret = await client.return_val(i)
                except mqttjsonrpc.jsonrpc.JsonRpcTimeout as e:
                    print(f"Get {e} during call")
                    fail_calls += 1
                    continue
                assert ret == i
                call_ok = True
                ok_calls += 1

        await server.stop()
        await client.stop()
        mqtt_broker.stop_testing()
        print(f"Test finished. Successful calls: {ok_calls} failed calls: {fail_calls}")

    async def get_msg(self, topic, port=1883, timeout=5):
        if not hasattr(self, "_get_msg_id"):
            self._get_msg_id = 0
        self._get_msg_id += 1
        try:
            async with asyncio.timeout(timeout):
                async with aiomqtt.Client('127.0.0.1', port, protocol=3) as client:
                    await client.subscribe(topic)
                    async for message in client.messages:
                        return message.payload
        except asyncio.TimeoutError as e:
            return None


    @pytest.mark.asyncio
    async def test_status(self, basic_json_rpc, mqtt_broker):
        mqtt_broker.start()
        await asyncio.sleep(1)

        mqtt_params = {'host': '127.0.0.1', 'protocol': 3, 'port': mqtt_broker.port}
        server = mqttjsonrpc.amqttrpc.AMqttRpcServer(basic_json_rpc, mqtt_params, SERVER_TOPIC)
        await server.start()
        await asyncio.sleep(5)
        status = await self.get_msg(SERVER_TOPIC + '/status', port=mqtt_broker.port)
        assert status == b'ONLINE'

        await server.stop()
        await asyncio.sleep(5)
        status = await self.get_msg(SERVER_TOPIC + '/status', port=mqtt_broker.port)
        assert status == b'OFFLINE'

        await server.start()
        await asyncio.sleep(5)
        status = await self.get_msg(SERVER_TOPIC + '/status', port=mqtt_broker.port)
        assert status == b'ONLINE'
        await server.stop()
        await asyncio.sleep(5)
        status = await self.get_msg(SERVER_TOPIC + '/status', port=mqtt_broker.port)
        assert status == b'OFFLINE'

        pr = PortRedirect(1234, mqtt_broker.port)
        pr.start()
        await asyncio.sleep(5)
        mqtt_params['port'] = 1234
        server = mqttjsonrpc.amqttrpc.AMqttRpcServer(basic_json_rpc, mqtt_params, SERVER_TOPIC)
        await server.start()
        await asyncio.sleep(5)
        status = await self.get_msg(SERVER_TOPIC + '/status', port=mqtt_broker.port)
        assert status == b'ONLINE'
        pr.stop()
        import os
        # os.system("killall -9 nc")
        await asyncio.sleep(5)

        status = await self.get_msg(SERVER_TOPIC + '/status', port=mqtt_broker.port)
        assert status == b'FAILED'

        await server.stop()
        mqtt_broker.stop_testing()