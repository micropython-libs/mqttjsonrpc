import sys
import asyncio

import logging
import mqttmgr

from mqttjsonrpc.jsonrpc import JsonRpc, JsonRpcException, JsonUnknownCommand, JsonRemoteException
from mqttjsonrpc.umqttrpc import UMqttRpcClient

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.handlers[0].setLevel(logging.DEBUG)
class ProxyHandler(logging.Handler):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
    def emit(self, record):
        self.parent.log(record.levelno, f"{record.name}:{record.message}")
proxy_handler = ProxyHandler(logger)
for log_name, log in logging._loggers.items():
    if not proxy_handler in log.handlers:
        if log.name == "root":
            continue
        log.addHandler(proxy_handler)

SERVER_TOPIC = "demo_server"
mqtt_params = {"host": "broker.hivemq.com"}

json_rpc = JsonRpc()

async def basic_test():
    mqtt_mgr = mqttmgr.MqttMgr("127.0.0.1",
                               "user",
                               "passwd",
                               client_id=f'test_mqtt_client',
                               #mqtt_prefix="test_server",
                               )
    mqtt_mgr.connect()
    print("MQTT Connected")
    await asyncio.sleep(1)
    umqttcliend = UMqttRpcClient(json_rpc, mqtt_mgr, SERVER_TOPIC + "/rpc")
    print(f"UMqttRpcClient Created {umqttcliend}")

    # Manual call
    print("Calling empty")
    await umqttcliend.empty()
    print("Called empty")
    print("Calling return_val")
    ret = await umqttcliend.return_val(5)
    print(f"Called return_val(5)={ret}")
    assert ret == 5
    print("Calling return_val with bytes")
    ret = await umqttcliend.return_val(b'asdf')
    print(f"Called return_val(b'asdf')={ret}")
    assert ret == b'asdf'
    print("Calling raise_str('Test exception')")
    # Test command that raise exception
    try:
        await umqttcliend.raise_str("Test exception")
    except JsonRemoteException as e:
        print(f'Received exception {type(e)} {e}')
        exp = e
    assert isinstance(exp, JsonRemoteException)
    # Test unknown command
    try:
        await umqttcliend.not_exist()
    except JsonUnknownCommand as e:
        print(f'Received exception {type(e)} {e}')
        exp = e
    assert isinstance(exp, JsonUnknownCommand)
    print("Stop server")
    umqttcliend.exit()

try:
    asyncio.run(basic_test())
    print("basic_test finished")
except Exception as e:
    print(f"Test failed, received exception {e} of type {type(e)})")
    sys.exit(1)
print("mptest_umqttrpc finished")
sys.exit(0)