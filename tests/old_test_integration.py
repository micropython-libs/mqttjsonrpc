import asyncio
import json
import os

import time
import logging
import time

logger = logging.getLogger(__name__)

import pytest
import pytest_asyncio
import mqttools
import aiomqtt
import mqttjsonrpc
from mqttjsonrpc.amqttrpc import AMqttRpcClient, AMqttRpcServer

if os.name == 'nt':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

BROKER_PORT = 1884

TEST_REPETITION_CNT = 1000
ASK_TOPIC = "ask/topic"
LISTEN_TOPIC = "listen/topic"
RESP_TOPIC = "resp/topic"
@pytest.fixture
def basic_json_rpc():
    json_rpc = mqttjsonrpc.jsonrpc.JsonRpc()
    json_rpc.add_func("empty", lambda : None)
    json_rpc.add_func("return_val", lambda x: x)
    json_rpc.add_func("raise_str", lambda x: exec('raise ValueError(f"raise for value {x}")'))
    return json_rpc

class TestIntegration:
    @pytest.mark.asyncio
    async def test_basic_server(self, basic_json_rpc):
        mqtt_broker = mqttools.BrokerThread(('0.0.0.0', BROKER_PORT))
        mqtt_broker.start()
        await asyncio.sleep(3)

        mqtt_params = {"host": "localhost", "protocol": 5}
        server = mqttjsonrpc.amqttrpc.AMqttRpcServer(basic_json_rpc, mqtt_params, LISTEN_TOPIC)
        await server.start()
        await asyncio.sleep(3)

        async with aiomqtt.Client('localhost', BROKER_PORT, protocol=5) as client:
            async with client.messages() as messages:

                empty_call = json.dumps(basic_json_rpc.create_call(RESP_TOPIC, "empty"))
                await client.subscribe(RESP_TOPIC)
                await client.publish(LISTEN_TOPIC, payload=empty_call)
                message = await anext(messages)
                assert str(message.topic) == RESP_TOPIC
                assert message.payload

                empty_call = json.dumps(basic_json_rpc.create_call(RESP_TOPIC, "return_val", args=[42]))
                await client.subscribe(RESP_TOPIC)
                await client.publish(LISTEN_TOPIC, payload=empty_call)
                message = await anext(messages)
                response = json.loads(message.payload)
                assert str(message.topic) == RESP_TOPIC
                assert message.payload
                assert response["value"] == 42

                empty_call = json.dumps(basic_json_rpc.create_call(RESP_TOPIC, "raise_str", args=[42]))
                await client.subscribe(RESP_TOPIC)
                await client.publish(LISTEN_TOPIC, payload=empty_call)
                message = await anext(messages)
                response = json.loads(message.payload)
                assert str(message.topic) == RESP_TOPIC
                assert message.payload
                assert "exception" in response
                assert response["exception"]["type"] == "ValueError"
                assert response["exception"]["msg"] == "raise for value 42"
        await server.stop()
        mqtt_broker.stop()
        await asyncio.sleep(1)

    @pytest.mark.asyncio
    async def test_basic_client(self, basic_json_rpc):
        mqttjsonrpc.amqttrpc.loop = asyncio.get_running_loop()
        mqtt_broker = mqttools.BrokerThread(('0.0.0.0', BROKER_PORT))
        mqtt_broker.start()
        await asyncio.sleep(3)

        mqtt_params = {"host": "localhost", "protocol": 5}
        client = AMqttRpcClient(basic_json_rpc, mqtt_params, ASK_TOPIC)
        await client.start()

        # Prepare server for responses
        server = AMqttRpcServer(basic_json_rpc, mqtt_params, ASK_TOPIC)
        await server.start()
        await asyncio.sleep(3)

        with pytest.raises(mqttjsonrpc.jsonrpc.JsonRpcException) as e:
            ret = await client.non_existent()
            assert ret is None

        ret = await client.return_val(44)
        assert ret == 44

        await client.stop()
        await server.stop()
        mqtt_broker.stop()
        await asyncio.sleep(1)

    @pytest.mark.asyncio
    async def test_repeated_call(self, basic_json_rpc):
        # Prepare environment
        mqttjsonrpc.amqttrpc.loop = asyncio.get_running_loop()
        mqtt_broker = mqttools.BrokerThread(('0.0.0.0', BROKER_PORT))
        mqtt_broker.start()
        await asyncio.sleep(3)
        mqtt_params = {"host": "localhost", "protocol": 5}
        client = AMqttRpcClient(basic_json_rpc, mqtt_params, ASK_TOPIC)
        await client.start()
        # Prepare server for responses
        server = AMqttRpcServer(basic_json_rpc, mqtt_params, ASK_TOPIC)
        await server.start()
        await asyncio.sleep(3)

        start = time.time()
        for i in range(TEST_REPETITION_CNT):
            with pytest.raises(mqttjsonrpc.jsonrpc.JsonRpcException) as e:
                ret = await client.non_existent()
                assert ret is None
            ret = await client.return_val(i)
            assert ret == i
        duration = time.time()-start
        print(f"Test took {duration}s {duration/TEST_REPETITION_CNT/2}s/call")
        # Test that everything was processed
        assert client.req_new.empty() and not client.req_waiting
        ### assert client.subscribed == {}
        for k, v in client.subscribed.items():
            assert v == 0
        assert len(client.req_waiting) == 0

        # Deinit enviroment
        await client.stop()
        await server.stop()
        mqtt_broker.stop()
        await asyncio.sleep(1)

    @pytest.mark.asyncio
    async def test_parallel_call(self, basic_json_rpc):
        # Prepare environment
        mqttjsonrpc.amqttrpc.loop = asyncio.get_running_loop()
        mqtt_broker = mqttools.BrokerThread(('0.0.0.0', BROKER_PORT))
        mqtt_broker.start()
        await asyncio.sleep(3)
        mqtt_params = {"host": "localhost", "protocol": 5}
        client = AMqttRpcClient(basic_json_rpc, mqtt_params, ASK_TOPIC)
        await client.start()
        # Prepare server for responses
        server = AMqttRpcServer(basic_json_rpc, mqtt_params, ASK_TOPIC)
        await server.start()
        await asyncio.sleep(3)

        async def except_calls(rep):
            for i in range(rep):
                with pytest.raises(mqttjsonrpc.jsonrpc.JsonRpcException) as e:
                    ret = await client.non_existent()
                    assert ret is None

        async def normal_calls(rep):
            for i in range(rep):
                ret = await client.return_val(i)
                assert ret == i

        start = time.time()
        except_task = asyncio.create_task(except_calls(1000))
        normal_task = asyncio.create_task(normal_calls(1000))
        await asyncio.gather(except_task, normal_task)
        duration = time.time() - start
        print(f"Test took {duration}s {duration / TEST_REPETITION_CNT / 2}s/call")
        # Test that everything was processed
        assert client.req_new.empty() and not client.req_waiting

        # Deinit enviroment
        await client.stop()
        await server.stop()
        mqtt_broker.stop()
        await asyncio.sleep(1)

    @pytest.mark.asyncio
    async def test_client_timeout(self, basic_json_rpc):
        mqttjsonrpc.amqttrpc.loop = asyncio.get_running_loop()
        mqtt_broker = mqttools.BrokerThread(('0.0.0.0', BROKER_PORT))
        mqtt_broker.start()
        await asyncio.sleep(3)

        mqtt_params = {"host": "localhost", "protocol": 5, "keepalive": 5, "timeout": 5}
        client = AMqttRpcClient(basic_json_rpc, mqtt_params, ASK_TOPIC)
        await client.start()

        # Prepare server for responses
        #server = mqttjsonrpc.AMqttRpcServer(basic_json_rpc, mqtt_params, ASK_TOPIC)
        #await server.start()
        #await asyncio.sleep(3)

        with pytest.raises(mqttjsonrpc.jsonrpc.JsonRpcTimeout) as e:
            ret = await client.non_existent()
            assert ret is None
        with pytest.raises(mqttjsonrpc.jsonrpc.JsonRpcTimeout) as e:
            ret = await client.return_val(44)
            assert ret == 44

        await client.stop()
        mqtt_broker.stop()
        await asyncio.sleep(1)

    @pytest.mark.asyncio
    async def test_client_server_broker_restart(self, basic_json_rpc):
        mqttjsonrpc.amqttrpc.loop = asyncio.get_running_loop()
        mqtt_broker = mqttools.BrokerThread(('0.0.0.0', BROKER_PORT))
        mqtt_broker.start()
        await asyncio.sleep(3)

        mqtt_params = {"host": "localhost", "protocol": 5, "keepalive": 5, "timeout": 5}
        client = AMqttRpcClient(basic_json_rpc, mqtt_params, ASK_TOPIC)
        await client.start()
        server = AMqttRpcServer(basic_json_rpc, mqtt_params, ASK_TOPIC)
        await server.start()
        await asyncio.sleep(5)

        x = 42
        ret = await client.return_val(x)
        assert ret == x
        await asyncio.sleep(1)

        logger.info("Shuting down brooker")
        mqtt_broker.stop()
        logger.info("Shut down brooker")
        await asyncio.sleep(20)

        logger.info("Call that will fail")
        with pytest.raises(mqttjsonrpc.jsonrpc.JsonRpcTimeout) as e:
            x = 43
            ret = await client.return_val(x)

        await asyncio.sleep(5)
        mqtt_broker = mqttools.BrokerThread(('0.0.0.0', BROKER_PORT))
        mqtt_broker.start()
        logger.info("New brooker")
        await asyncio.sleep(10)

        logger.info("Working calls")
        x = 44
        ret = await client.return_val(x)
        assert ret == x

        x = 45
        ret = await client.return_val(x)
        assert ret == x

        logger.info("Stop")
        # Deinit enviroment
        await client.stop()
        await server.stop()
        await asyncio.sleep(1)
        mqtt_broker.stop()
        await asyncio.sleep(1)
