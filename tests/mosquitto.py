import time
import subprocess
import threading
import random
import argparse

class Mosquitto:
    def __init__(self, port = None, params = None):
        self.process = None
        self.thread = None
        self.port = port or random.randint(1025, 2**16-1)
        self.params = params or []

        self.on_interval = 1.0
        self.off_interval = 0.1
        self._stop = False

    def __del__(self):
        if self.thread:
            self.stop_testing()

    def is_running(self):
        if self.process is None:
            return False
        else:
            return not self.process.poll()

    def start(self, params=None):
        params = self.params + (params or [])
        if self.port:
            params += ["-p", f"{self.port}"]
        print("Starting mosquitto")
        self.process = subprocess.Popen(["mosquitto"] + params)

    def stop(self):
        if self.process:
            print("Stopping mosquitto")
            self.process.kill()

    def _blink_thread(self):
        while not self._stop:
            self.start()
            time.sleep(self.on_interval)
            self.stop()
            time.sleep(self.off_interval)
        self.stop()

    def start_testing(self, on_interval=None, off_interval=None):
        if on_interval is not None:
            self.on_interval = on_interval
        if off_interval is not None:
            self.off_interval = off_interval
        self.stop_testing()
        self._stop = False
        self.thread = threading.Thread(target=self._blink_thread)
        self.thread.start()

    def stop_testing(self):
        if self.thread is not None:
            self._stop = True
            self.thread.join()
            self.thread = None

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("online_time", help="How long should mqtt run", default=5, type=float)
    parser.add_argument("offline_time", help="How long should be mqtt stopped", default=1, type=float)
    parser.add_argument("--port", help="mqtt port", default=1883)
    args = parser.parse_args()

    mqtt_broker = Mosquitto(port=args.port)
    mqtt_broker.start_testing(args.online_time, args.offline_time)
    time.sleep(30*60)