import sys
import asyncio
import time

import logging
import mqttmgr

from mqttjsonrpc.jsonrpc import JsonRpc
from mqttjsonrpc.umqttrpc import UMqttRpcServer

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.handlers[0].setLevel(logging.DEBUG)
class ProxyHandler(logging.Handler):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
    def emit(self, record):
        self.parent.log(record.levelno, f"{record.name}:{record.message}")
proxy_handler = ProxyHandler(logger)
for log_name, log in logging._loggers.items():
    if not proxy_handler in log.handlers:
        if log.name == "root":
            continue
        log.addHandler(proxy_handler)

SERVER_TOPIC = "demo_server"
mqtt_params = {"host": "broker.hivemq.com"}

terminate_server = asyncio.Event()

json_rpc = JsonRpc()
json_rpc.add_func("empty", lambda : print(f"Called empty()"))
json_rpc.add_func("return_val", lambda x: x)
json_rpc.add_func("raise_str", lambda x: exec(f'raise ValueError(f"raise for value {x}")'))
json_rpc.add_func("exit", lambda: terminate_server.set())

async def basic_test():
    mqtt_mgr = mqttmgr.MqttMgr("127.0.0.1",
                               "user",
                               "passwd",
                               client_id=f'test_mqtt_server',
                               )
    mqtt_mgr.connect()
    print("MQTT Connected")
    # await asyncio.sleep(5)
    umqtt_server = UMqttRpcServer(json_rpc, mqtt_mgr, SERVER_TOPIC)
    await umqtt_server.start()
    print(f"UMqttRpcServer Created {umqtt_server}")
    await terminate_server.wait()

try:
    asyncio.run(basic_test())
    print("basic_test finished")
except Exception as e:
    print(f"Test failed, received exception {e} of type {type(e)})")
    sys.print_exception(e)
print("mptest_umqttrpc finished")
sys.exit(0)