import json
import copy
import tracemalloc
from base64 import b64encode, b64decode
tracemalloc.start()

import pytest
from unittest.mock import MagicMock, AsyncMock, ANY, patch
import mqttjsonrpc
import mqttjsonrpc.umqttrpc as umqttrpc

@pytest.fixture
def client_rpc():
    return mqttjsonrpc.jsonrpc.JsonRpc()

import asyncio

# Workaround to be able to unit test micropython code
async def _wait_for_ms(fut, timeout):
    await asyncio.wait_for(fut, timeout/1000)

asyncio.wait_for_ms = _wait_for_ms

class TestRpcJson:
    def test_create_empty_call(self, mocker, client_rpc):
        with patch("mqttjsonrpc.jsonrpc.get_rand_id") as _get_rand_id:
            _get_rand_id.return_value="asdf"
            call_rpc = client_rpc.create_call("asdf", "simple_func")
            assert (call_rpc == {"id": "asdf", "resp_top": "asdf", "fn": "simple_func"})

    def test_create_simple_call(self, mocker, client_rpc):
        with patch("mqttjsonrpc.jsonrpc.get_rand_id") as _get_rand_id:
            _get_rand_id.return_value="asdf"
            args = ["a", 1, 1.0, None]
            call_rpc = client_rpc.create_call("resp/top", "simple_func", args)
            assert (call_rpc == {"id": "asdf", "resp_top": "resp/top", "fn": "simple_func",
                                 "args": args})
            kwargs = {"a": 1, 1.0: 1, None: None, "x": "y"}
            call_rpc = client_rpc.create_call("resp/top", "simple_func", kwargs=kwargs)
            assert (call_rpc == {"id": "asdf", "resp_top": "resp/top", "fn": "simple_func",
                                 "kwargs": kwargs})
            call_rpc = client_rpc.create_call("resp/topic", "simple_func", args=args, kwargs=kwargs)
            assert (call_rpc == {"id": "asdf", "resp_top": "resp/topic", "fn": "simple_func",
                                 "args": args, "kwargs": kwargs})

    def test_create_bytes_call(self, mocker, client_rpc):
        with patch("mqttjsonrpc.jsonrpc.get_rand_id") as _get_rand_id:
            _get_rand_id.return_value="asdf"
            args = ["a", 1, 1.0, None, b"asdf1234"]
            kwargs = {"a": 1, 1.0: 1, None: None, "x": "y", "base64_param": b"asdf1234"}
            call_rpc = client_rpc.create_call("resp/topic", "simple_func", args=args, kwargs=kwargs)
            assert (call_rpc == {"id": "asdf", "resp_top": "resp/topic", "fn": "simple_func",
                                 "args": args, "kwargs": kwargs})

    def test_handle_empty_call(self, mocker):
        server = mqttjsonrpc.jsonrpc.JsonRpc()
        basic_function = mocker.MagicMock(return_value="ret_val")
        server.add_func("basic_function", basic_function)
        ret = server.handle_call({"id": "RandomId", "resp_top": "response/topic",
                                  "fn": "basic_function"})
        assert ret == {"id": "RandomId", "value": "ret_val"}

    def test_handle_basic_call(self, mocker):
        server = mqttjsonrpc.jsonrpc.JsonRpc()
        basic_function = mocker.MagicMock(return_value="ret_val")
        server.add_func("basic_function", basic_function)
        # Test args
        args_func = mocker.MagicMock(return_value=1)
        server.add_func("args_func", args_func)
        ret = server.handle_call({"id": "ABC123", "resp_top": "resp/topic", "fn": "args_func",
                                  "args": [1, "a", [1, 2], ["b", 1.0]]})
        assert ret == {"id": "ABC123", "value": 1}
        args_func.assert_called_with(*[1, "a", [1, 2], ["b", 1.0]])
        args_func.all_count = 1
        # Test args, argw
        args_argw_func = mocker.MagicMock(return_value=[1, 2])
        server.add_func("args_akwarg_func", args_argw_func)
        ret = server.handle_call({"id": "ABC1234", "resp_top": "resp/topic", "fn": "args_akwarg_func",
                                  "args": [1, "a", [1, 2], ["b", 1.0]],
                                  "kwargs": {"a": 1, "x": "a"}})
        assert ret == {"id": "ABC1234", "value": [1, 2]}
        args_argw_func.assert_called_with(*[1, "a", [1, 2], ["b", 1.0]], **{"a": 1, "x": "a"})
        args_argw_func.all_count = 1

    def test_handle_base64_call(self, mocker):
        server = mqttjsonrpc.jsonrpc.JsonRpc()
        # Test args, argw
        args_argw_func = mocker.MagicMock(return_value=[1, 2])
        server.add_func("args_akwarg_func", args_argw_func)
        ret = server.handle_call(mqttjsonrpc.jsonrpc.encode_objects(
            {"id": "ABC1234", "resp_top": "resp/topic", "fn": "args_akwarg_func",
             "args": [1, "a", [1, 2], ["b", 1.0], b"asdf1234"],
             "kwargs": {"a": 1, "x": "a", "base64_param": b"asdf1234"}}))
        assert ret == {"id": "ABC1234", "value": [1, 2]}
        args_argw_func.assert_called_with(*[1, "a", [1, 2], ["b", 1.0], b"asdf1234"],
                                          **{"a": 1, "x": "a", "base64_param": b"asdf1234"})
        args_argw_func.all_count = 1

    def test_handle_exception_call(self, mocker):
        server = mqttjsonrpc.jsonrpc.JsonRpc()
        exception_function = mocker.MagicMock(return_value=None)
        exception_function.side_effect = RuntimeError("Test error")
        server.add_func("exception_function", exception_function)
        ret = server.handle_call({"id": "ABC1234", "resp_top": "resp/topic", "fn": "exception_function"})
        assert ret == {"id": "ABC1234", "exception": {"type": "RuntimeError", "msg": "Test error"}}

    def test_encode_simple(self):
        json_rpc = mqttjsonrpc.jsonrpc.JsonRpc()
        basic_data = {"a": "b", 1: 2, True: [1, 2, 3], None: None}
        mqttjsonrpc.jsonrpc.encode_objects(basic_data)
        assert basic_data == {"a": "b", 1: 2, True: [1, 2, 3], None: None}

    def test_encode_bytes(self):
        json_rpc = mqttjsonrpc.jsonrpc.JsonRpc()
        basic_data = {"a": b'asdf1234', True: [1, b'asdf1234', 3], None: b'asdf1234'}
        enc_data = mqttjsonrpc.jsonrpc.encode_objects(basic_data)
        enc = {"enc_data": b64encode(b'asdf1234').decode('ascii'), "enc_type": "b64"}
        assert enc_data == {"a": enc, True: [1, enc, 3], None: enc}

    def test_decode(self):
        json_rpc = mqttjsonrpc.jsonrpc.JsonRpc()
        basic_data = {"a": b'asdf1234', True: [1, b'asdf1234', 3], None: b'asdf1234'}
        enc = {"enc_data": b64encode(b'asdf1234').decode('ascii'), "enc_type": "b64"}
        basic_enc_data = {"a": enc, True: [1, enc, 3], None: enc}
        mqttjsonrpc.jsonrpc.decode_objects(basic_enc_data) # Decode data in place
        assert basic_data == basic_enc_data

    def test_encode_decode(self):
        json_rpc = mqttjsonrpc.jsonrpc.JsonRpc()
        basic_data = {"a": b'asdf1234', True: [1, b'asdf1234', 3], None: b'asdf1234', 1.0: [{"X": b'asdf\x00\x01\xff'}]}
        ret = mqttjsonrpc.jsonrpc.encode_objects(copy.deepcopy(basic_data))
        ret = mqttjsonrpc.jsonrpc.decode_objects(ret)
        assert ret == basic_data

class TestFuncProxy:
    @pytest.mark.asyncio
    async def test_empty_call(self, mocker):
        fake_parent = mocker.MagicMock()
        fake_parent.make_call = AsyncMock(return_value=None)

        func_proxy = mqttjsonrpc.FuncProxy(fake_parent)

        ret = await func_proxy.empty()
        assert ret is None
        fake_parent.make_call.assert_called_with("empty", (), {}, ask_topic=None)


    @pytest.mark.asyncio
    async def test_simple_call(self, mocker):
        fake_parent = mocker.MagicMock()
        fake_parent.make_call = AsyncMock(return_value=None)

        func_proxy = mqttjsonrpc.FuncProxy(fake_parent)

        ret = await func_proxy.print("Test")
        assert ret is None
        fake_parent.make_call.assert_called_with("print", ("Test", ), {}, ask_topic=None)

    @pytest.mark.asyncio
    async def test_complex_call(self, mocker):
        fake_parent = mocker.MagicMock()
        fake_parent.make_call = AsyncMock(return_value=42)

        func_proxy = mqttjsonrpc.FuncProxy(fake_parent)

        ret = await func_proxy.complex("param1", "param2", color="black")
        assert ret == 42
        fake_parent.make_call.assert_called_with("complex", ("param1", "param2"),
                                                 {"color": "black"}, ask_topic=None)


ASK_TOPIC = "aks/topic"
@pytest.fixture
def mqtt_rpc_client(client_rpc, mocker):
    mqtt_mgr_mock = mocker.MagicMock()
    mqtt_mgr_mock.add_callback = mocker.MagicMock()
    mqtt_mgr_mock.publish = mocker.MagicMock()
    client_rpc.id_len=0
    return umqttrpc.UMqttRpcClient(client_rpc, mqtt_mgr_mock, ASK_TOPIC)

class TestAMqttRequestResponse:
    @pytest.mark.asyncio
    async def test_simple(self):
        with patch("mqttjsonrpc.jsonrpc.get_rand_id") as _get_rand_id:
            _get_rand_id.return_value = "asdf"

            mqtt_mgr = MagicMock()
            mqtt_mgr.add_callback = MagicMock()
            mqtt_mgr.publish = MagicMock()
            req_resp = umqttrpc._UMqttRequestResponse(mqtt_mgr, resp_topic="resp/topic")
            test_msg = '{"TEST": "test"}'
            req_resp.event.wait = AsyncMock()
            req_resp.response = "{}"
            await req_resp.wait_response("asdf", test_msg)

            mqtt_mgr.add_callback.assert_called_with(b"resp/topic", ANY)
            mqtt_mgr.publish.assert_called_with("asdf", test_msg, 0)

class TestAMqttRpcClient:
    @pytest.mark.asyncio
    async def test_empty_call(self, mocker, mqtt_rpc_client, client_rpc):  # rpc_resp
        with (patch("asyncio.Event") as event, patch("mqttjsonrpc.jsonrpc.get_rand_id") as _get_rand_id,
              patch("mqttjsonrpc.jsonrpc.get_rand_letters") as _get_rand_letters):
            _get_rand_id.return_value = "asdfid"
            _get_rand_letters.return_value = "aBc"

            client_req = client_rpc.create_call("aBc", "test_fnc")
            rpc_resp = json.dumps({"id": "asdfid", "value": None})
            # We need to call callback right after event.wait()
            instance = event.return_value
            instance.wait = AsyncMock()
            async def _call_cb():
                # Activate given callback on event.wait
                mqtt_rpc_client.mqtt.add_callback.call_args[0][1](
                    mqtt_rpc_client.mqtt.add_callback.call_args[0][0],
                    rpc_resp,
                )
            instance.wait.side_effect = _call_cb
            # Test begin
            ret = await mqtt_rpc_client.test_fnc()

        mqtt_rpc_client.mqtt.add_callback.assert_called_with(b"aBc", ANY)
        mqtt_rpc_client.mqtt.publish.assert_called_with(ASK_TOPIC, json.dumps(client_req), 1)

    @pytest.mark.asyncio
    async def test_basic_call(self, mocker, mqtt_rpc_client, client_rpc):  # rpc_resp
        with (patch("asyncio.Event") as event, patch("mqttjsonrpc.jsonrpc.get_rand_id") as _get_rand_id,
              patch("mqttjsonrpc.jsonrpc.get_rand_letters") as _get_rand_letters):
            _get_rand_id.return_value = "asdfid"
            _get_rand_letters.return_value = "aBc"

            client_req = client_rpc.create_call("aBc", "test_fnc") # , args = [1, "a", [1, 2], ["b", 1.0], b"asdf1234"], kwargs = {"a": 1, "x": "a", "base64_param": b"asdf1234"}
            rpc_resp = json.dumps({"id": "asdfid", "value": None})
            # We need to call callback right after event.wait()
            instance = event.return_value
            instance.wait = AsyncMock()
            async def _call_cb():
                # Activate given callback on event.wait
                mqtt_rpc_client.mqtt.add_callback.call_args[0][1](
                    mqtt_rpc_client.mqtt.add_callback.call_args[0][0],
                    rpc_resp,
                )
            instance.wait.side_effect = _call_cb
            # Test begin
            ret = await mqtt_rpc_client.test_fnc()

        mqtt_rpc_client.mqtt.add_callback.assert_called_with(b"aBc", ANY)
        mqtt_rpc_client.mqtt.publish.assert_called_with(ASK_TOPIC,
            json.dumps(mqttjsonrpc.jsonrpc.encode_objects(client_req)), 1)

    @pytest.mark.asyncio
    async def test_empty_proxy_call(self, mocker, mqtt_rpc_client, client_rpc):
        with (patch("asyncio.Event") as event, patch("mqttjsonrpc.jsonrpc.get_rand_id") as _get_rand_id,
              patch("mqttjsonrpc.jsonrpc.get_rand_letters") as _get_rand_letters):
            _get_rand_id.return_value = "asdfid"
            _get_rand_letters.return_value = "aBc"

            client_req = client_rpc.create_call("aBc", "test_fnc")
            rpc_resp = json.dumps({"id": "asdfid", "value": None})
            # We need to call callback right after event.wait()
            instance = event.return_value
            instance.wait = AsyncMock()
            async def _call_cb():
                # Activate given callback on event.wait
                mqtt_rpc_client.mqtt.add_callback.call_args[0][1](
                    mqtt_rpc_client.mqtt.add_callback.call_args[0][0],
                    rpc_resp,
                )
            instance.wait.side_effect = _call_cb
            # Test begin
            ret = await mqtt_rpc_client.call(ask_topic=ASK_TOPIC).test_fnc()

        mqtt_rpc_client.mqtt.add_callback.assert_called_with(b"aBc", ANY)
        mqtt_rpc_client.mqtt.publish.assert_called_with(ASK_TOPIC, json.dumps(client_req), 1)

    @pytest.mark.asyncio
    async def test_empty_proxy_call_change_topic(self, mocker, mqtt_rpc_client, client_rpc):
        with (patch("asyncio.Event") as event, patch("mqttjsonrpc.jsonrpc.get_rand_id") as _get_rand_id,
              patch("mqttjsonrpc.jsonrpc.get_rand_letters") as _get_rand_letters):
            _get_rand_id.return_value = "asdfid"
            _get_rand_letters.return_value = "aBc"

            client_req = client_rpc.create_call("aBc", "test_fnc")
            rpc_resp = json.dumps({"id": "asdfid", "value": None})
            # We need to call callback right after event.wait()
            instance = event.return_value
            instance.wait = AsyncMock()
            async def _call_cb():
                # Activate given callback on event.wait
                mqtt_rpc_client.mqtt.add_callback.call_args[0][1](
                    mqtt_rpc_client.mqtt.add_callback.call_args[0][0],
                    rpc_resp,
                )
            instance.wait.side_effect = _call_cb
            # Test begin
            ret = await mqtt_rpc_client.call(ask_topic="different/topic").test_fnc()

        mqtt_rpc_client.mqtt.add_callback.assert_called_with(b"aBc", ANY)
        mqtt_rpc_client.mqtt.publish.assert_called_with("different/topic", json.dumps(client_req), 1)
@pytest.fixture
def server_rpc():
    return mqttjsonrpc.jsonrpc.JsonRpc()

class TestAMqttRpcServer:
    @pytest.mark.asyncio
    async def test_init_disable(self, server_rpc):
        mqtt_mgr = MagicMock()
        mqtt_mgr.add_callback = MagicMock()
        mqtt_mgr.del_callback = MagicMock()
        # Init
        rpc_server = umqttrpc.UMqttRpcServer(server_rpc, mqtt_mgr, "listen/top")
        await rpc_server.start()
        mqtt_mgr.add_callback.assert_called_with(b"listen/top/rpc", rpc_server._serve_req)
        # Turn off
        await rpc_server.stop()
        mqtt_mgr.del_callback.assert_called_with(b"listen/top/rpc", rpc_server._serve_req)

    @pytest.mark.asyncio
    async def test_basic_disable(self, server_rpc):
        mqtt_mgr = MagicMock()
        mqtt_mgr.add_callback = MagicMock()
        mqtt_mgr.del_callback = MagicMock()

        rpc_server = umqttrpc.UMqttRpcServer(server_rpc, mqtt_mgr, "listen/top")
        basic_func = MagicMock()
        rpc_server.add_func(basic_func, "basic_func")


