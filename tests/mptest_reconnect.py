import sys
import asyncio
import time
import errno

import logging
import mqttmgr

from mqttjsonrpc.jsonrpc import JsonRpc, JsonRpcTimeout
from mqttjsonrpc.umqttrpc import UMqttRpcServer, UMqttRpcClient

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.handlers[0].setLevel(logging.DEBUG)
class ProxyHandler(logging.Handler):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
    def emit(self, record):
        self.parent.log(record.levelno, f"{record.name}:{record.ct}-{record.message}")
proxy_handler = ProxyHandler(logger)
for log_name, log in logging._loggers.items():
    print(f'Logger {log_name} {log}')
    if not proxy_handler in log.handlers:
        if log.name == "root":
            continue
        log.addHandler(proxy_handler)
        print(f'add {proxy_handler} to {log}')

SERVER_TOPIC = "demo_server"
mqtt_params = {"host": "broker.hivemq.com"}

terminate_server = asyncio.Event()

def return_val(val):
    logger.debug(f"{time.time()} Received return_val call ({val})")
    return val

json_rpc = JsonRpc()
json_rpc.add_func("empty", lambda : print(f"Called empty()"))
json_rpc.add_func("return_val", return_val)
json_rpc.add_func("raise_str", lambda x: exec(f'raise ValueError(f"raise for value {x}")'))
json_rpc.add_func("exit", lambda: terminate_server.set())

async def basic_test():
    mqtt_mgr = mqttmgr.MqttMgr("127.0.0.1",
                               "user",
                               "passwd",
                               client_id=f'test_mqtt_server',
                               msg_sleep=1,
                               reconnect_timeout=1_000,
                               )
    mqtt_mgr.connect()
    print("MQTT Connected")
    # await asyncio.sleep(5)
    umqtt_server = UMqttRpcServer(json_rpc, mqtt_mgr, SERVER_TOPIC)
    await umqtt_server.start()
    print(f"UMqttRpcServer Created {umqtt_server}")
    umqttcliend = UMqttRpcClient(json_rpc, mqtt_mgr, SERVER_TOPIC + "/rpc")

    test_cnt = 3_000
    ok_calls = 0
    fail_calls = 0
    start = time.time()
    for i in range(test_cnt):
        call_ok = False
        while not call_ok:
            try:
                logger.debug(f"{time.time()} Send request umqttcliend.return_val({i})")
                ret = await umqttcliend.return_val(i)
                logger.debug(f"{time.time()} Received response from umqttcliend.return_val({i})")
            except JsonRpcTimeout as e:
                print(f"Get {e} during call")
                fail_calls += 1
                continue
            except OSError as e:
                print(f"OSError exception {e} ({type(e)}) errno:{e.errno} - {errno.errorcode[e.errno]}")
                fail_calls += 1
                continue
            except Exception as e:
                print(f"Unexpected exception {e} ({type(e)})")
                fail_calls += 1
                continue
            assert ret == i
            call_ok = True
            ok_calls += 1

    await umqtt_server.stop()
    duration = time.time() - start
    print(f"Finished {ok_calls} calls and failed {fail_calls} calls in {duration} ({duration/ok_calls*1000}ms/msg)")


try:
    asyncio.run(basic_test())
    print("basic_test finished")
except Exception as e:
    print(f"Test failed, received exception {e} of type {type(e)})")
    sys.print_exception(e)
    sys.exit(1)
print("mptest_umqttrpc finished")
sys.exit(0)