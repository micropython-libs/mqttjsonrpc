#!/usr/bin/env python

from distutils.core import setup

setup(name='mqttjsonrpc',
      version='0.1.0',
      description='Python/Micropython class that provide simple RPC client/server over mqtt using json',
      author='Jan Klusáček',
      author_email='honza.klugmail.com',
      url='https://gitlab.com/users/honza.klu',
      packages=['mqttjsonrpc'],
      install_requires=['aiomqtt==2.0.0'],
     )