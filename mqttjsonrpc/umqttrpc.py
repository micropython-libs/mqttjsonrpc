import json
import logging
import asyncio
import json

import mqttjsonrpc.jsonrpc as jsonrpc
from mqttjsonrpc import AMqttServerBase, AMqttClientBase

logger = logging.getLogger(__name__)


def _str_to_bytes(value):
    if isinstance(value, (bytes, bytearray)):
        return value
    else:
        return value.encode('utf8')

class UMqttRpcServer(AMqttServerBase):
    def __init__(self, json_rpc: jsonrpc.JsonRpc, mqtt_mgr, listen_topic):
        super().__init__(json_rpc)
        self.mqtt_mgr = mqtt_mgr
        self._listen_topic = listen_topic

        self.state = "stopped"

    def __del__(self):
        # self.stop() # Cannot await in destructor
        pass

    async def start(self):
        await self._install_callback()
        self.mqtt_mgr.publish(self._listen_topic + "/status", "ONLINE", retain=True)
        self.mqtt_mgr.mqtt.set_last_will(self._listen_topic + "/status", "FAILED", retain=True)
        self.state = "running"

    async def stop(self):
        await self._uninstall_callback()
        self.mqtt_mgr.publish(self._listen_topic + "/status", "OFFLINE", retain=True)
        self.state = "stopped"

    def _serve_req(self, topic: str, msg: str): # TODO: Convert this to async
        logger.debug(f"Received request {topic} {msg}")
        try:
            data = json.loads(msg)
            resp_top = data["resp_top"]
        except Exception as e:
            logger.error(f"Critical error ({e}) when loading rpc json {msg} on topic {topic}")
        ret = self.json_rpc.handle_call(data)
        self.mqtt_mgr.publish(resp_top, json.dumps(ret))

    async def _install_callback(self):
        self.mqtt_mgr.add_callback(_str_to_bytes(self._listen_topic + "/rpc"),
                                   self._serve_req)  # TODO: Make this async (both add_callback and _serve_req)

    async def _uninstall_callback(self):
        self.mqtt_mgr.del_callback(_str_to_bytes(self._listen_topic + "/rpc"), self._serve_req) # TODO: Does this work?

    def add_func(self, func, func_name=None):
        func_name = func_name or func.__name__
        self.json_rpc.add_func(func_name, func)

    def del_func(self, func_name):
        self.json_rpc.del_func(func_name)


class _UMqttRequestResponse:
    def __init__(self, mqtt_mgr, resp_topic: str, id_len: int=8, timeout: int=1000):
        self.mqtt_mgr = mqtt_mgr
        self.id_len = id_len
        self.resp_topic = _str_to_bytes(resp_topic)
        self.timeout = timeout

        self.event = asyncio.Event()
        self.response = None

    def _call_back(self, topic: str, message: str):
        print(f"_call_back(self, {topic}, {message})")
        if topic != self.resp_topic:
            logger.error(f"Unexpected message id when waiting for response {topic} != {self.resp_topic}")
        self.response = message
        self.event.set()

    async def wait_response(self, req_topic: str, req_msg: str, qos=0):
        if self.resp_topic is None:
            self.resp_topic = _str_to_bytes(jsonrpc.get_rand_letters(self.id_len))
        self.mqtt_mgr.add_callback(self.resp_topic, self._call_back)
        self.mqtt_mgr.publish(req_topic, req_msg, qos)
        try:
            await asyncio.wait_for_ms(self.event.wait(), self.timeout)
        except asyncio.TimeoutError as e:
            print(f"Exception {e} type:{type(e)}")
            raise jsonrpc.JsonRpcTimeout(e)
        finally:
            self.mqtt_mgr.del_callback(self.resp_topic, self._call_back)
        # TODO: Remove callback
        return json.loads(self.response)


class UMqttRpcClient(AMqttClientBase):
    _RequestResponse = None

    def __init__(self, json_rpc: jsonrpc.JsonRpc, mqtt, ask_topic: str | None = None, qos=1):
        """

        :param json_rpc: JsonRpc object
        :param mqtt object for handling mqtt communication
        :param ask_topic: topic used for sending rpc (topic used by MqttRpcServer)

        """
        super().__init__(json_rpc, ask_topic)
        self.mqtt = mqtt
        self.qos = qos

        self.id_len = 8

    async def make_call(self, func_name: str, args: list, kwargs: dict, ask_topic=None):
        print(f"make_call {func_name} {args} {kwargs}")
        resp_topic = jsonrpc.get_rand_letters(self.id_len)
        req_resp = _UMqttRequestResponse(self.mqtt, resp_topic=resp_topic)
        json_call = self.json_rpc.create_call(resp_topic, func_name, args, kwargs)
        #try:
        #    json_call_str = json.dumps(json_call)
        #except TypeError as e:
        #    json_call_str = json.dumps(jsonrpc.encode_objects(json_call))
        # Micropython version must encode every time because json.dumps never fails in micropython
        json_call_str = json.dumps(jsonrpc.encode_objects(json_call))
        print(f"req_resp.wait_response({self.ask_topic}, {json_call}, qos={self.qos})")
        resp = await req_resp.wait_response(ask_topic or self.ask_topic, json_call_str, qos=self.qos) # TODO: rename ask topic?
        print("req_resp.wait_response finished")

        if json_call["id"] != resp["id"]:
            raise ValueError(f'id in request and response does not match {json_call["id"]}!={resp["id"]}')
        if "exception" in resp:
            if resp["exception"]["type"] == "JsonUnknownCommand":
                raise jsonrpc.JsonUnknownCommand(resp["exception"]["msg"])
            elif resp["exception"]["type"] == "JsonInternalError":
                raise jsonrpc.JsonInternalError(resp["exception"]["msg"])
            else:
                raise jsonrpc.JsonRemoteException(f'{resp["exception"]["type"]}: {resp["exception"]["msg"]}')
        return jsonrpc.decode_objects(resp["value"])

    def __getattr__(self, item_name):
        async def call_fnc(*args, **kwargs):
            return await self.make_call(item_name, args, kwargs)
        return call_fnc
