import logging
import random
import json

import logging

if hasattr(logging, "getLogger"):
    logger = logging.getLogger(__name__)
else:
    logger = logging.Logger(__name__)
try:
    import aiomqtt
except ImportError:
    aiomqtt = None
# AsyncIO
try:
    import asyncio
except ImportError:
    asyncio = None
import mqttjsonrpc.jsonrpc as jsonrpc


class AMqttServerBase:
    """ Base class for asynchronous Mqtt servers

    Have to contain self.json_rpc: json rpc
    """
    def __init__(self, json_rpc: jsonrpc.JsonRpc):
        self.json_rpc = json_rpc

    async def start(self):
        raise NotImplementedError()

    async def stop(self):
        raise NotImplementedError()

    def add_func(self, func, func_name=None):
        func_name = func_name or func.__name__
        self.json_rpc.add_func(func_name, func)

    def del_func(self, func_name):
        self.json_rpc.del_func(func_name)


class AMqttClientBase:
    def __init__(self, json_rpc, ask_topic):
        self.json_rpc = json_rpc
        self.ask_topic = ask_topic

    def call(self, ask_topic=None):
        return FuncProxy(self, ask_topic)

    async def make_call(self, func_name, args, kwargs, ask_topic=None):
        raise RuntimeError("make_call not implemented")

    def __getattr__(self, item_name):
        async def call_fnc(*args, **kwargs):
            return await self.make_call(item_name, args, kwargs)
        return call_fnc


# TODO: Not used?
class FuncProxy:
    def __init__(self, parent, ask_topic=None):
        self.parent = parent
        self.ask_topic = ask_topic

    async def _make_call(self, func_name, args, kwargs):
        logging.debug(f"Called: {func_name} with args:{args} kwargs:{kwargs}")
        return await self.parent.make_call(func_name, args, kwargs, ask_topic=self.ask_topic)

    def __getattr__(self, item_name):
        logging.debug(item_name)

        async def ret(*args, **kwargs):
            r = await self._make_call(item_name, args, kwargs)
            return r
        return ret
