import asyncio
import logging
import json

import aiomqtt

import mqttjsonrpc.jsonrpc as jsonrpc
from mqttjsonrpc import AMqttServerBase, AMqttClientBase

logger = logging.getLogger(__name__)

class _AMqttRequestResponse:
    def __init__(self, ask_topic, resp_topic, json):
        self.ask_topic = ask_topic
        self.topic = resp_topic # TODO: Rename to resp topic
        self.json = json
        self.resp = None
        self.exception = None

        self.done = asyncio.Event()


class AMqttRpcClient(AMqttClientBase):
    def __init__(self, json_rpc, mqtt_params, ask_topic: str | None = None, timeout=5, qos=1):
        """

        :param json_rpc: JsonRpc object
        :param mqtt object for handling mqtt communication
        :param ask_topic: topic used for sending rpc (topic used by MqttRpcServer)

        """
        super().__init__(json_rpc, ask_topic)
        self.mqtt_params = mqtt_params
        self.timeout = timeout
        self.qos = qos

        self.id_len = 8
        self._mqtt_task = None
        self._client_id = jsonrpc.get_rand_letters(self.id_len)

        # For communication with _mqtt_task
        self.req_new = asyncio.Queue() # queue of requests waiting to be processed
        self.req_waiting = set() # queue of requests waiting for response
        self.subscribed = {}

    async def mqtt_send_handling(self, client):
        """ Task for sending messages through mqtt """
        logger.debug(f"Started AMqttRpcClient.mqtt_send_handling")
        while True:
            try:
                # Send request if there is some waiting
                req = await self.req_new.get()
                ### if req.topic not in subscribed:
                if not self.subscribed.get(req.topic):
                    await client.subscribe(req.topic)
                    self.subscribed[req.topic] = 1
                    self.req_waiting.add(req)
                else:
                    self.subscribed[req.topic] += 1
                try:
                    json_data = json.dumps(req.json)
                except TypeError as e:
                    json_data = json.dumps(jsonrpc.encode_objects(req.json))
                await client.publish(req.ask_topic, json_data)
            except asyncio.CancelledError as e:
                logger.debug(f"mqtt_send_handling task canceled")
                return
            except Exception as e:
                logger.error(f"Error when sending data ({e})")
                break

    async def mqtt_handling(self):
        while True:
            self.subscribed = {}
            send_task = None
            try:
                async with aiomqtt.Client(self.mqtt_params["host"],
                                          self.mqtt_params.get("port", 1883),
                                          timeout=10,
                                          keepalive=self.mqtt_params.get("keepalive") or 60,
                                          username=self.mqtt_params.get("user"),
                                          password=self.mqtt_params.get('password'),
                                          protocol=self.mqtt_params.get("protocol"),
                                          identifier=self._client_id,
                                          ) as client:
                    logger.debug(f"Connected mqtt")
                    send_task = asyncio.create_task(self.mqtt_send_handling(client))
                    to_delete = None
                    async for msg in client.messages:
                        # Find response
                        resp = json.loads(msg.payload)
                        for wait_idx, req in enumerate(self.req_waiting):
                            if str(msg.topic) == req.topic:
                                if resp["id"] != req.json["id"]:
                                    logger.debug(f"Received message {resp} on {msg.topic}" 
                                                    f"with unexpected id {resp['id']}!={req.json['id']}")
                                    continue
                                to_delete = req
                                break
                        else:
                            logger.error(f"Received message {msg.payload} on unexpected topic {msg.topic}")
                        if to_delete is not None:
                            req.resp = resp
                            self.req_waiting.remove(req)
                            req.done.set()
                            self.subscribed[req.topic] -= 1
                            # Unsubscribe if none other use same topic
                            unsub = not any([(r.topic == req.topic) for r in self.req_waiting])
                            if unsub:
                                await client.unsubscribe(req.topic)
                                assert self.subscribed[req.topic] == 0

            except asyncio.CancelledError:
                if send_task is not None:
                    send_task.cancel()
                    await send_task
                    send_task = None
                    self._mqtt_task = None
                return
            except Exception as e:
                logger.error(f"Mqtt rpc client task failed with {e}")
                if send_task is not None:
                    send_task.cancel()
                    await send_task
                    send_task = None
                await asyncio.sleep(0.1)

    async def make_call(self, func_name, args, kwargs, ask_topic=None):
        await self.start()
        resp_topic = jsonrpc.get_rand_letters(self.id_len)
        json_call = self.json_rpc.create_call(resp_topic, func_name, args, kwargs)
        req = _AMqttRequestResponse(ask_topic or self.ask_topic, resp_topic, json_call)
        await self.req_new.put(req)
        try:
            await asyncio.wait_for(req.done.wait(), timeout=self.timeout)
        except asyncio.TimeoutError:
            if req in self.req_waiting:
                self.req_waiting.remove(req)
                if req.topic in self.subscribed:
                    self.subscribed[req.topic] -= 1
            raise jsonrpc.JsonRpcTimeout()
        if req.exception:
            raise req.exception

        resp = req.resp
        if json_call["id"] != resp["id"]:
            raise ValueError(f'id in request and response does not match {json_call["id"]}!={resp["id"]}')
        if "exception" in resp:
            if resp["exception"]["type"] == "JsonUnknownCommand":
                raise jsonrpc.JsonUnknownCommand(resp["exception"]["msg"])
            elif resp["exception"]["type"] == "JsonInternalError":
                raise jsonrpc.JsonInternalError(resp["exception"]["msg"])
            else:
                raise jsonrpc.JsonRemoteException(f'{resp["exception"]["type"]}: {resp["exception"]["msg"]}')
        ret = jsonrpc.decode_objects(resp["value"])
        return ret

    async def start(self):
        if self._mqtt_task is None:
            self._mqtt_task = asyncio.create_task(self.mqtt_handling())

    async def stop(self):
        if self._mqtt_task is None:
            return
        # Cancel new requests
        try:
            while True:
                req = self.req_new.get_nowait()
                req.exception = jsonrpc.JsonCanceledError("Client is stopped")
                req.done.set()
                logger.debug(f"Removing item ({req}) from req_new")
        except asyncio.QueueEmpty:
            pass
        # Cancel requests waiting for response
        while not self.req_new.empty() or self.req_waiting:
            await asyncio.sleep(self.timeout)
            logger.debug("Waiting for req_waiting to empty")
            for req in self.req_waiting:
                req.exception = jsonrpc.JsonCanceledError("Client is stopped")
                req.done.set()
            self.req_waiting = set()

        if self.mqtt_task is not None:
            self._mqtt_task.cancel()
            await self._mqtt_task
            self._mqtt_task = None


class AMqttRpcServer(AMqttServerBase):
    def __init__(self, json_rpc, mqtt_params, listen_topic):
        super().__init__(json_rpc)
        self.mqtt_params = mqtt_params
        self._listen_topic = listen_topic
        self._status_topic = self._listen_topic + "/status"

        self._client_id = jsonrpc.get_rand_id(8)
        self.state = "stopped"
        self.mqtt_task = None

    async def start(self):
        if self.mqtt_task is not None:
            raise ValueError("Starting mqtt_task that is already started")
        self.mqtt_task = asyncio.get_running_loop().create_task(self._mqtt_task())

    async def stop(self):
        if self.mqtt_task is not None:
            self.mqtt_task.cancel()
            await self.mqtt_task
        self.mqtt_task = None

    async def _mqtt_task(self):
        while True:
            try:
                async with aiomqtt.Client(self.mqtt_params["host"],
                                          self.mqtt_params.get("port", 1883),
                                          timeout=1.0, # TODO configurable timeout
                                          keepalive=self.mqtt_params.get("keepalive") or 60,
                                          username=self.mqtt_params.get("user"),
                                          password=self.mqtt_params.get('password'),
                                          protocol=self.mqtt_params.get("protocol"),
                                          identifier=self._client_id,
                                          will=aiomqtt.Will(topic=self._status_topic, payload='FAILED', retain=True),
                                          ) as client:  # TODO: Add ssl support
                    await client.publish(self._status_topic, payload="ONLINE", retain=True)
                    try:
                        await client.subscribe(self._listen_topic + "/rpc")
                        self.state = "running"
                        async for message in client.messages:
                            if str(message.topic) != self._listen_topic + "/rpc":
                                raise ValueError(f"Unexpected AMqttRpcServer topic {self._listen_topic}")
                            try:
                                data = json.loads(message.payload)
                                resp_top = data["resp_top"]
                            except Exception as e:
                                logger.error(f"AMqttRpcServer Critical error ({e}) when loading rpc json {message.payload} on topic {message.topic}")
                            ret = json.dumps(self.json_rpc.handle_call(data))
                            await client.publish(resp_top, ret)
                    except asyncio.CancelledError as e:
                        await client.publish(self._status_topic, payload="OFFLINE", retain=True)
                        raise e
            except asyncio.CancelledError as e:
                logger.debug("Mqtt server task was canceled")
                return
            except Exception as e:
                self.state = "failed"
                logger.error(f"Mqtt server task failed with {e}")
                await asyncio.sleep(0.1)

    async def stop(self):
        if self.mqtt_task is not None:
            self.mqtt_task.cancel()
        try:
            await self.mqtt_task
        except asyncio.CancelledError:
            pass
        self.mqtt_task = None

    async def _install_callback(self):
        if self.mqtt_task != None:
            raise ValueError("Starting mqtt_task that is already started")
        self.mqtt_task = asyncio.create_task(self._mqtt_task())

    async def _uninstall_callback(self):
        self.mqtt_task.cancel()
