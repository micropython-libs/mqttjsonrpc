try:
    from base64 import b64encode, b64decode
except ImportError:
    from ubinascii import b2a_base64 as b64encode, a2b_base64 as b64decode
import random

ascii_letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

def get_rand_letters(top_len):
    return "".join([ascii_letters[random.randrange(len(ascii_letters))] for _ in range(top_len)])


def get_rand_id(id_len):
    return b64encode(bytes([random.randrange(256) for _ in range(id_len)])).decode("ascii")


class JsonRpcException(Exception):
    pass


class JsonRpcTimeout(JsonRpcException):
    pass


class JsonUnknownCommand(JsonRpcException):
    """ Raised when requested command is not defined

    """
    pass


class JsonRemoteException(JsonRpcException):
    """ Raised when called command raised exception

    """
    pass


class JsonInternalError(JsonRpcException):
    """ Raised by interval error (micformated json etc)

    """
    pass


class JsonCanceledError(JsonInternalError):
    pass

def _is_iterable(obj):
    try:
        iter(obj)
        return True
    except TypeError:
        return False

def encode_objects(val):
    """ Replace object that cannot be encoded to json

    Returns copy of modified val
    """
    if isinstance(val, (type(None), bool, int, float, str,)):
        return val
    if isinstance(val, (bytes, bytearray)):
        return {'enc_data': b64encode(val).decode('ascii'), 'enc_type': 'b64'}
    elif _is_iterable(val) and not hasattr(val, 'keys'):
        ret = []
        for v in val:
            ret.append(encode_objects(v))
        return ret
    elif hasattr(val, 'items'):
        ret = {}
        for k, v in list(val.items()):
            ret[encode_objects(k)] = encode_objects(v)
        return ret
    else:
        raise TypeError(f"Unsupported type {type(val)}")

def decode_objects(val):
    """ Replace object that were removed by encode_objects

    Modify objects in place
    """
    if isinstance(val, (type(None), bool, int, float, str,)):
        return val
    elif _is_iterable(val) and not hasattr(val, 'keys'):
        for i, v in enumerate(val):
            val[i] = decode_objects(v)
        return val
    elif hasattr(val, 'items'):
        # Decode dict given in
        if len(val) == 2 and 'enc_data' in val and 'enc_type' in val:
            if val['enc_type'] == 'b64':
                return b64decode(val['enc_data'])
            else:
                raise TypeError(f'Unknown enc_type {val["enc_type"]}')
        for k in list(val.keys()):
            v = val[k]
            del val[k]
            val[decode_objects(k)] = decode_objects(v)
        return val

class JsonRpc:
    def __init__(self, id_len=8):
        """

        :param pub_cb: publish callback expecting publish(msg, topic)
        :param sub_cb: subscribe callback expecting sub_cb(topic, cb)
        """
        self._functions = {}
        self.id_len = int(id_len)

    def add_func(self, name, func):
        self._functions[name] = func

    def del_func(self, name):
        del self._functions[name]

    def handle_call(self, json_call):
        exc = None
        json_call = decode_objects(json_call)
        try:
            if "id" not in json_call:
                raise JsonInternalError("Missing id in json call")
            if "resp_top" not in json_call:
                raise JsonInternalError("Missing resp_top in json call")
            if "fn" not in json_call:
                raise JsonInternalError("Missing fn in json call")
            if json_call["fn"] not in self._functions:
                raise JsonUnknownCommand(f"Called unknown function {json_call['fn']}")
            func = self._functions[json_call["fn"]] # TODO: Handle async functions?
            args = json_call.get("args", [])
            kwargs = json_call.get("kwargs", {})
            ret = func(*args, **kwargs)
        except Exception as e:
            exc = e
        resp = {"id": json_call["id"]}
        if exc is None:
            resp["value"] = encode_objects(ret)
        else:
            # TODO: Distinguish different exceptions generated by server
            resp["exception"] = {"type": exc.__class__.__name__, "msg": str(exc)}
        return resp

    def create_call(self, resp_topic, func_name, args=None, kwargs=None):
        call_json = {"id": get_rand_id(self.id_len), "resp_top": resp_topic, "fn": func_name}
        if args:
            call_json["args"] = args
        if kwargs:
            call_json["kwargs"] = kwargs
        return call_json
