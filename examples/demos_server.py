import sys
import asyncio
if sys.platform == 'win32':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

from mqttjsonrpc.amqttrpc import AMqttRpcServer
from mqttjsonrpc.jsonrpc import JsonRpc

SERVER_TOPIC = "demo_server"
mqtt_params = {"host": "127.0.0.1"} #TODO: Get this from parameter

terminate_server = asyncio.Event()

async def demo_server_task():
    json_rpc = JsonRpc()
    json_rpc.add_func("empty", lambda : None)
    json_rpc.add_func("return_val", lambda x: x)
    json_rpc.add_func("return_str", lambda x: str(x))
    json_rpc.add_func("raise_str", lambda x: exec('raise ValueError(f"raise for value {x}")'))
    json_rpc.add_func("exit", lambda: terminate_server.set())

    server = AMqttRpcServer(json_rpc, mqtt_params, SERVER_TOPIC)
    await server.start()
    print("Server is running")
    await terminate_server.wait()
    print("Server is requested to stop")
    await server.stop()
    print("Server finishing")

asyncio.run(demo_server_task())