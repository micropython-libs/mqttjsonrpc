import sys
import asyncio
import time

if sys.platform == 'win32':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

from mqttjsonrpc.amqttrpc import AMqttRpcClient
from mqttjsonrpc.jsonrpc import JsonRpc, JsonRpcException, JsonUnknownCommand, JsonRemoteException

SERVER_TOPIC = "demo_server/rpc"
mqtt_params = {"host": "127.0.0.1"} #TODO: Get this from parameter

async def demo_server_task():
    json_rpc = JsonRpc()

    client = AMqttRpcClient(json_rpc, mqtt_params, SERVER_TOPIC)
    await client.start()

    print("Manual calls")
    # Test make call
    ret = await client.make_call("empty", [], {})
    print(f"called empty() = {ret}")
    assert ret == None
    ret = await client.make_call("return_val", ["XXX"], {})
    print(f"called return_val('XXX') = {ret}")
    assert ret == "XXX"

    print("Transparent calls")
    # Test transparent calls
    ret = await client.empty()
    print(f"client.empty() = {ret}")
    assert ret == None
    ret = await client.return_val(1.0)
    print(f"client.return_val(1.0)")
    assert ret == 1.0
    ret = await client.return_val(b'asdf123')
    print(f"client.return_val(b'asdf123')")
    assert ret == b'asdf123'
    try:
        ret = await client.raise_str("Test error")
        raise ValueError("Should raise")
    except JsonRemoteException as e:
        print(f"Exception captured {e}")
        exp = e
    print(f'Tested client.raise_str("Test error")')
    assert isinstance(exp, JsonRemoteException)

    try:
        ret = await client.not_exist()
    except JsonUnknownCommand as e:
        print(f"Exception captured {e}")
        exp = e
    print(f'client.not_exist()')
    assert isinstance(exp, JsonUnknownCommand)

    ret = await client.exit()
    assert ret is None

    print(f'Stopping client')
    await client.stop()
    print(f'Client stopped')


asyncio.run(demo_server_task())